package com.boot.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.template.source.StringSource;
/**
 * 将sql文件资源添加到ActiveRecordPlugin中
 * @author 李文斌
 *
 */
public class SqlResourceResolver {
	private static final Logger logger=LoggerFactory.getLogger(SqlResourceResolver.class);
	/**
	 * 添加sql模板
	 * @param arp
	 * @param sqlFilePath
	 */
	public static void addSqlTemplate(ActiveRecordPlugin arp,String sqlFilePath) {
		try {
			ResourcePatternResolver resolver=new PathMatchingResourcePatternResolver();
			Resource[] resources = resolver.getResources(sqlFilePath+File.separator+"*.sql");
			for (Resource resource : resources) {
				StringBuilder contentByStream = getContentByStream(resource.getInputStream());
				arp.addSqlTemplate(new StringSource(contentByStream.toString(), true));
				logger.info("添加[{}]到ActiveRecordPlugin",resource.getFilename());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 读取内容
	 * @param inputStream
	 * @return
	 */
	private static StringBuilder getContentByStream(InputStream inputStream) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
			String line;
			while ((line = br.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringBuilder;
	}

}
