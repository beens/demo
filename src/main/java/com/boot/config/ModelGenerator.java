package com.boot.config;

import javax.sql.DataSource;


import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class ModelGenerator {
    private static String username="root";

    private static String password="123456";

    private static String url="jdbc:mysql://127.0.0.1:3306/study?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8";
    
	private static DruidPlugin createDruidPlugin() {
		return new DruidPlugin(url, username, password.trim());
	}

	public static DataSource getDataSource() {
		DruidPlugin druidPlugin = createDruidPlugin();
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}

	/**
	 * 数据库发生变化时 执行一次该方法
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Model保存路径
		String modelPackageName = "com.boot.model";
		// baseModel保存路径
		String baseModelPackageName = modelPackageName + ".base";
		String modelPath = modelPackageName.replaceAll("\\.", "\\\\");
		String modelOutputDir = PathKit.getWebRootPath() + "\\src\\main\\java\\" + modelPath;
		String baseModelOutputDir = modelOutputDir + "\\base";
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName,
				modelOutputDir);
		generator.setGenerateChainSetter(true);
		generator.addExcludedTable("adv");
		generator.setGenerateDaoInModel(true);
		generator.setGenerateChainSetter(true);
		generator.setGenerateDataDictionary(true);
		// 省略前缀
		generator.setRemovedTableNamePrefixes("t_");
		generator.generate();
	}
}
