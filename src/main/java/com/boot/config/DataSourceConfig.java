package com.boot.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import com.alibaba.druid.pool.DruidDataSource;
import com.boot.model._MappingKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;

/**
 * @ClassName: DataSourceConfig.java
 * @Description: 数据库链接配置
 * @version: v1.0.0
 * @author: 小李子
 * @date: 2019年10月28日 下午3:24:52
 */
@Configuration
public class DataSourceConfig {
	private final Logger logger = LoggerFactory.getLogger(DataSourceConfig.class);

	/**
	 * 初始化连接池
	 * 
	 * @return
	 */
	@Bean
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource druidDataSource() {
		logger.info("初始化数据源");
		return new DruidDataSource();
	}

	/**
	 * 设置数据源代理
	 */
	@Bean
	public TransactionAwareDataSourceProxy transactionAwareDataSourceProxy() {
		logger.info("设置数据源代理");
		TransactionAwareDataSourceProxy transactionAwareDataSourceProxy = new TransactionAwareDataSourceProxy();
		transactionAwareDataSourceProxy.setTargetDataSource(druidDataSource());
		return transactionAwareDataSourceProxy;
	}

	public static void main(String[] args) {
		try {
			ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
			Resource[] resources = resolver.getResources("/sql/*.sql");
			for (Resource resource : resources) {
				String filename = resource.getFilename();
				System.out.println(filename);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置ActiveRecord
	 */
	@Bean
	public ActiveRecordPlugin activeRecordPlugin() {
		logger.info("设置ActiveRecord");
		ActiveRecordPlugin arp = new ActiveRecordPlugin(transactionAwareDataSourceProxy());
		arp.setDialect(new MysqlDialect());
		arp.setShowSql(true);
		//多个sql文件自动装载
		SqlResourceResolver.addSqlTemplate(arp, "sql");
		//sql文件映射器
		_MappingKit.mapping(arp);
		arp.start();
		return arp;

	}


	/**
	 * 设置事务管理
	 */
	@Bean
	public DataSourceTransactionManager dataSourceTransactionManager() {
		logger.info("设置事务管理器");
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(transactionAwareDataSourceProxy());
		return dataSourceTransactionManager;
	}

}
