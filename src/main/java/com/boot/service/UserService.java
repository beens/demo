package com.boot.service;

import com.boot.base.ResultBean;

public interface UserService {
	
	public ResultBean findByUserName(String username)throws Exception;

}
