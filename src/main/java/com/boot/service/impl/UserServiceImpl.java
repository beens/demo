package com.boot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.boot.base.ResultBean;
import com.boot.dao.UserDao;
import com.boot.service.UserService;

@Service
@Transactional//使用事务  可以注解到类也可以注解到方法
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	
	@Override
	public ResultBean findByUserName(String username) throws Exception {
		
		return ResultBean.success(userDao.findByUserName(username));
	}

}
