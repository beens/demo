package com.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boot.base.BaseController;
import com.boot.base.ResultBean;
import com.boot.service.UserService;

@RestController
@RequestMapping("user")
public class UserController extends BaseController{

	@Autowired
	private UserService userService;
	
	
	@RequestMapping("findByUserName")
	public ResultBean findByUserName(String username) {
		try {
			return userService.findByUserName(username);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("根据用户名查找用户时出错",e);
			return ResultBean.error("系统错误，请重试");
		}
	}
	
}
