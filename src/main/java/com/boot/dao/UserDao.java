package com.boot.dao;

import org.springframework.stereotype.Repository;

import com.boot.base.BaseDao;
import com.boot.model.User;

@Repository
public class UserDao extends BaseDao<User>{
	private final User dao=User.dao;
	
	public User findByUserName(String username) {
		return dao.findFirst(dao.getSql("user.findByUserName"),username);
	}

}
