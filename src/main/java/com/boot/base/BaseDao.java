package com.boot.base;



import com.jfinal.plugin.activerecord.Model;


public class BaseDao<M extends Model<?>> {


	public final boolean insert(M model) {
		if (model == null) {
			return false;
		}
		return model.save();
	}

	public final boolean update(M model) {
		if (model == null) {
			return false;
		}
		return model.update();
	}

	public final boolean delete(M model) {
		if (null == model) {
			return false;
		}
		return model.delete();
	}
	

}
