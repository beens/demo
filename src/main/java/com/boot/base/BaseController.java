package com.boot.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @ClassName: BaseController.java
* @Description:控制层基类
*
* @version: v1.0.0
* @author: 小李子
* @date: 2019年11月4日 上午10:31:16
 */
public class BaseController {
	protected final Logger logger=LoggerFactory.getLogger(getClass());
}
